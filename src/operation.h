#ifndef __OPERATION_H
#define __OPERATION_H

#include <handle.h>

class Forest;
class Cache;

enum fddl_result { SUCCESS = 0, TUPLE_OUT_OF_BOUNDS, INVALID_MDD, OPERATION_FAILED, INVALID_LEVEL };
typedef std::string OperationException;

class Operation{
	protected:
		Cache** m_cache;
		Forest* m_forest;
	public:	
		Operation(Forest* forest);
		~Operation();
};
#endif //__OPERATION_H
