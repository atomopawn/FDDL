#ifndef ARRAY_OPERATION_H
#define ARRAY_OPERATION_H

#include <types.h>
#include <handle.h>
#include <operation.h>

class Forest;

class ArrayOperation : public Operation
{
	protected:
	virtual node_idx base_case(const node_idx p[], const unsigned int size) const = 0;
	node_idx apply(const level k, const node_idx p[], const unsigned int size);

	public:
	ArrayOperation(Forest* forest);

	fddl_result apply(const MDDHandle handles[], const unsigned int size, MDDHandle& result);
};

#endif  //ARRAY_OPERATION_H
