/* 
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef CACHENODE_H
#define CACHENODE_H

#include <types.h>

class CacheNode 
{
	protected:
	//Next pointer for linked list
	CacheNode* m_next;
	
	//Operation result
	node_idx m_result;

	public:
	CacheNode(const node_idx result);

	//Accessors
	CacheNode* next() const;
	node_idx result() const;
		
	//Mutators
	void set_next(CacheNode* const& next);

	//Pure virtual functions that must be implemented in subclass
	virtual bool operator==(const CacheNode& other) const = 0;
	virtual unsigned int hash() const = 0;
};

class UnaryNode : public CacheNode
{
	private:
	node_idx m_p;

	public:
	UnaryNode(const node_idx p, const node_idx result);

	unsigned int hash() const;
	bool operator==(const CacheNode& other) const;
};

class BinaryNode : public CacheNode 
{
	private:
	node_idx m_p;
	node_idx m_q;

	public:
	BinaryNode(const node_idx p, const node_idx q, const node_idx result);

	unsigned int hash() const;
	bool operator==(const CacheNode& other) const;
};

class TernaryNode: public CacheNode
{
	private:
	node_idx m_p;
	node_idx m_q;
	node_idx m_s;

	public:
	TernaryNode(const node_idx p, const node_idx q, const node_idx s, const node_idx result);

	unsigned int hash() const;
	bool operator==(const CacheNode& other) const;
};

class TupleNode : public CacheNode
{
	private:
	node_idx* m_vals;
	unsigned int m_size;

	public:
	TupleNode(const node_idx * const vals, const unsigned int size, const node_idx result);
	TupleNode(const TupleNode&);
	~TupleNode();

	TupleNode& operator=(const TupleNode&);

	unsigned int hash() const;
	bool operator==(const CacheNode& other) const;
};

class SelectNode : public CacheNode
{
	private:
	node_idx m_p;
	node_idx* m_vals;
	unsigned int m_size;

	public:
	SelectNode(const node_idx p, const node_idx * const vals, const unsigned int size, const node_idx result);
	SelectNode(const SelectNode&);
	~SelectNode();

	SelectNode& operator=(const SelectNode&);

	unsigned int hash() const;
	bool operator==(const CacheNode& other) const;
};

#endif  //CACHENODE_H
