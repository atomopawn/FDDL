/*
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef MDDHANDLE_H
#define MDDHANDLE_H

#include <assert.h>
#include <string>

typedef std::string HandleException;

class MDDHandle {

 protected:
	int m_index;

 public:

	MDDHandle() {
		m_index = -1;
	} 

	MDDHandle(const MDDHandle& other) 
	{
		m_index = other.m_index;
	}

	bool operator==(const MDDHandle& other) const 
	{
		return other.m_index == this->m_index;
	}

	bool operator!=(const MDDHandle& other) const 
	{
		return !(*this == other);
	}

	MDDHandle operator=(MDDHandle a) {
		throw HandleException("Cannot assign MDD handles using = ");
	}

	MDDHandle operator=(int idx) {
		m_index = idx;
	}

	bool invalid() const 
	{
		return m_index == -1;
	}

	int index() const
	{
		return m_index;
	}
};

#endif //MDDHANDLE_H
