#include "forest.h"
#include "binaryoperation.h"

bool BinaryOperation::m_commutative = true;

BinaryOperation::BinaryOperation(Forest* forest) : Operation(forest)
{
}

fddl_result BinaryOperation::apply(MDDHandle h1, MDDHandle h2, MDDHandle &result)
{
	if (h1.invalid())
		return OPERATION_FAILED;

	if (h2.invalid())
		return OPERATION_FAILED;

	node_idx new_result = apply(m_forest->top_level(), h1.index(), h2.index());
	if (result.index() != new_result) {
		m_forest->ReallocHandle(result);
		m_forest->Attach(result, new_result);
	}
	return SUCCESS;
}

node_idx BinaryOperation::apply(const level k, const node_idx p, const node_idx q) 
{
	node_idx r = 0;
	if (k > m_forest->top_level())
		throw OperationException("Level out of range.\n");

	if (p == 0) {
		return p_zero(k,q);
	}
	else if (q == 0) {
		return q_zero(k,p);
	}

	if (k == 0) {
		return base_case(p,q);
	}

	r = m_cache[k]->hit(k, p, q);
	if (r >= 0) {
		//If the node has been deleted, restore it before returning.
		if (m_forest->FDDL_NODE(k, r).deleted())
			r = m_forest->CheckIn(k, r);
		return r;
	}

	r = m_forest->NewNode(k);

	Node* nodeP = &m_forest->FDDL_NODE(k,p);
	Node* nodeQ = &m_forest->FDDL_NODE(k,q);

	int psize = nodeP->m_size;
	int qsize = nodeQ->m_size;

	arc_idx i = 0;
	arc_idx j = 0;
	while (i < psize || j < qsize) {
		arc_idx pdx;
		arc_idx qdx;
		node_idx pval;
		node_idx qval;
	
		if (nodeP->sparse() && i < psize){
			pdx = m_forest->SPARSE_INDEX(k, nodeP, i);
			pval = m_forest->SPARSE_ARC(k, nodeP, i);	
		}
		else if (i < psize){
			pdx = i;
			pval = m_forest->FULL_ARC(k, nodeP, i);
		}

		if (nodeQ->sparse() && j < qsize) {
			qdx = m_forest->SPARSE_INDEX(k, nodeQ, j);
			qval = m_forest->SPARSE_ARC(k, nodeQ, j);	
		}
		else if (j < qsize){
			qdx = j;
			qval = m_forest->FULL_ARC(k, nodeQ, i);
		}

		if (i >= psize) {
			m_forest->SetArc(k, r, qdx, qval);
			++j;
		}
		else if (j >= qsize) {
			m_forest->SetArc(k, r, pdx, pval);
			++i;
		}
		else if (pdx < qdx) {
			m_forest->SetArc(k, r, pdx, pval);
			++i;
		}
		else if (qdx < pdx) {
			m_forest->SetArc(k, r, qdx, qval);
			++j;
		}
		else {
			m_forest->SetArc(k, r, pdx, apply(k-1, pval, qval));
			++i;
			++j;
		}
	}
	r = m_forest->CheckIn(k, r);
	m_cache[k]->add(k,p,q,r);
	if (m_commutative)
		m_cache[k]->add(k,q,p,r);
	return r;
}
