#include "unaryoperation.h"
#include "forest.h"

bool UnaryOperation::m_reflexive = true;

UnaryOperation::UnaryOperation(Forest* forest) : Operation(forest)
{
}

void UnaryOperation::visit(const level k, const node_idx p)
{	
}

fddl_result UnaryOperation::apply(MDDHandle h, MDDHandle &result)
{
	if (h.invalid())
		return OPERATION_FAILED;
	node_idx new_result = apply(m_forest->top_level(), h.index());
	if (result.index() != new_result) {
		m_forest->ReallocHandle(result);
		m_forest->Attach(result, new_result);
	}
	return SUCCESS;
}

node_idx UnaryOperation::apply(const level k, const node_idx p) 
{
	node_idx r = 0;
	if (k > m_forest->top_level())
		throw OperationException("Level out of range.\n");


	if (k == 0) {
		return base_case(p);
	}

	r = m_cache[k]->hit(k,p);
	if (r >= 0) {
		//If the cached node has been deleted, restore it before returning.
		if (m_forest->FDDL_NODE(k, r).deleted())
			r = m_forest->CheckIn(k, r);
		return r;
	}

	if (p == 0) {
		r = zero_case(k);
	}
	else {
		r = m_forest->NewNode(k);
		Node* nodeP = &m_forest->FDDL_NODE(k,p);
		int psize = nodeP->m_size;

		arc_idx i;
		arc_idx ival;
		node_idx idx;
		node_idx tmp;
		
		i = 0;
		while (i < psize) {
			if (nodeP->sparse()) {
				ival = m_forest->SPARSE_INDEX(k, nodeP, i);
				idx = m_forest->SPARSE_ARC(k, nodeP, i);
			}
			else {
				idx = i < psize ? m_forest->FULL_ARC(k, nodeP, i) : 0;
			}	
			tmp = apply(k-1, idx);
			if (nodeP->sparse()) {
				m_forest->SetArc(k, r, ival, tmp);
			}
			else {
				m_forest->SetArc(k, r, i, tmp);
			}
			++i;
		}
		if (nodeP->sparse()) {
			i = ival + 1;
		}

		//If the node doesn't span the range, we have more work to do.  The implicit arcs all point to zero.
		//@TODO: Add a boolean m_zero_maps_to_zero that is true when f(0)=0.  If true, skip this loop.
		tmp = zero_case(k-1);
		while (i <= m_forest->max_val(k)) {
			m_forest->SetArc(k, r, i, tmp);
			++i;
		}
	}
	r = m_forest->CheckIn(k, r);
	m_cache[k]->add(k,p,r);
	if (m_reflexive)
		m_cache[k]->add(k,r,p);
	visit(k,p);
	return r;
}
