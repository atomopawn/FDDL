#include "forest.h"
#include "arrayoperation.h"

ArrayOperation::ArrayOperation(Forest* forest) : Operation(forest)
{
}

fddl_result ArrayOperation::apply(const MDDHandle handles[], const unsigned int size, MDDHandle &result)
{
	node_idx nodes[size];
	for (unsigned int i=0; i < size; ++i) {
		if (handles[i].invalid())
			return OPERATION_FAILED;
		nodes[i] = handles[i].index();
	}

	node_idx new_result = apply(m_forest->top_level(), nodes, size);
	if (result.index() != new_result) {
		m_forest->ReallocHandle(result);
		m_forest->Attach(result, new_result);
	}
	return SUCCESS;
}

node_idx ArrayOperation::apply(const level k, const node_idx p[], const unsigned int size)
{
	node_idx r = 0;
	if (k > m_forest->top_level())
		throw OperationException("Level out of range.\n");

	if (k == 0) {
		return base_case(p, size);
	}

	r = m_cache[k]->hit(k, p, size);
	if (r >= 0) {
		//If the node has been deleted, restore it before returning.
		if (m_forest->FDDL_NODE(k, r).deleted())
			r = m_forest->CheckIn(k, r);
		return r;
	}

	r = m_forest->NewNode(k);

	int maxsize = 0;
	for (unsigned int i=0; i < size; ++i) {
		Node* node = &m_forest->FDDL_NODE(k,i);
		if (node->m_size > maxsize)
			maxsize = node->m_size;
	}

	Node* nodes[size];

	for (unsigned int i=0; i < size; ++i) {
		nodes[i] = &m_forest->FDDL_NODE(k,p[i]);
	}

	arc_idx index[size];
	for (unsigned int i=0; i < size; ++i) {
		index[i] = 0;
	}
	bool done = false;

	while (!done) {
		done = true;
		arc_idx idx[size];	
		node_idx val[size];
		for (unsigned int i=0; i < size; ++i) {
			if (index[i] < nodes[i]->size() && nodes[i]->sparse()){
				idx[i] = m_forest->SPARSE_INDEX(k, nodes[i], index[i]);
				val[i] = m_forest->SPARSE_ARC(k, nodes[i], index[i]);
				++index[i];
				done = false;
			}
			else if (index[i] < nodes[i]->size()){
				idx[i] = index[i];
				val[i] = m_forest->FULL_ARC(k, nodes[i], index[i]);
				++index[i];
				done = false;
			}
			else {
				idx[i] = -1;
				val[i] = 0;
			}
		}
		arc_idx mindx = 0;
		for (unsigned int i=1; i < size; ++i) {
			if (val[i] >= 0 && idx[i] < idx[mindx]) {
				mindx = i;
			}
		}
		for (unsigned int i=1; i < size; ++i) {
			if (idx[i] != idx[mindx])
				val[i] = 0;
		}
		if (idx[mindx] != 0) 
			m_forest->SetArc(k, r, idx[mindx], apply(k-1, val, size));
	}
	r = m_forest->CheckIn(k, r);
	m_cache[k]->add(k, p, size, r);
	return r;
}
