/*
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef NODE_H 
#define NODE_H

#include <handle.h>

//An MDD node consists of two parts, a node struct stored in a node
//array, and a set of arcs stored in a per-level arc array.
class Node {
	public:

	enum flag { FLAG_NONE=0, FLAG_PRINT=1, FLAG_SPARSE=2, FLAG_DELETED=4, FLAG_CHECKED_IN=8, FLAG_PRUNE=16 };

	int m_down;		//Index into the arc array of the first downpointer for this node
	int m_size;		//Number of arcs leaving this node.  If stored sparsely, the number of non-zero arcs.
	int m_in;		//Number of incoming Arcs (used for reference counting)

	Node() {
		m_flags = FLAG_NONE;
		m_down = -1;
		m_size = 0;
		m_in = 0;
	}


	bool test(flag f) const
	{
		return (m_flags & f) != 0;
	}

	bool print() const
	{
		return (m_flags & FLAG_PRINT) != 0;
	}

	bool sparse() const
	{
		return (m_flags & FLAG_SPARSE) != 0;
	}	
	
	bool deleted() const
	{
		return (m_flags & FLAG_DELETED) != 0;
	}

	bool checked_in() const
	{
		return (m_flags & FLAG_CHECKED_IN) != 0;
	}

	bool prune() const
	{
		return (m_flags & FLAG_PRUNE) != 0;
	}

	void clear_flags() 
	{
		m_flags = FLAG_NONE;
	}

	void set(flag f) {
		m_flags = static_cast<flag>(m_flags | f);
	}

	void clear(flag f) {
		m_flags = static_cast<flag>(m_flags & ~f);
	}

	void set_print() {
		set(FLAG_PRINT);
	}
	
	void clear_print() {
		clear(FLAG_PRINT);
	}

	void set_sparse() {
		set(FLAG_SPARSE);
	}

	void clear_sparse() {
		clear(FLAG_SPARSE);
	}

	void set_deleted() {
		set(FLAG_DELETED);
	}
	
	void clear_deleted() {
		clear(FLAG_DELETED);
	}

	void set_checked_in() {
		set(FLAG_CHECKED_IN);
	}

	void clear_checked_in() {
		clear(FLAG_CHECKED_IN);
	}

	void set_prune() {
		set(FLAG_PRUNE);
	}

	void clear_prune() {
		clear(FLAG_PRUNE);
	}

	int size() const
	{
		return m_size;
	}

	private:
	flag m_flags;		//Parameter flags (see above for values)
};

#endif  //NODE_H
