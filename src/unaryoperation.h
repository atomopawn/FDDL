#ifndef UNARY_OPERATION_H
#define UNARY_OPERATION_H

#include <forest.h>
#include <handle.h>
#include <operation.h>

class UnaryOperation : public Operation 
{
	protected:
	virtual node_idx base_case(const node_idx p) const = 0;
	virtual node_idx zero_case(const level k) const = 0;
	virtual void visit(const level k, const node_idx p);

	node_idx apply(const level k, const node_idx p);

	static bool m_reflexive;

	public:
	UnaryOperation(Forest* forest);

	fddl_result apply(MDDHandle h, MDDHandle& result);
};

#endif  //UNARY_OPERATION_H
