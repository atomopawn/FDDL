/*
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef TYPES_H
#define TYPES_H

typedef int level;		//Level of the MDD
typedef int node_idx;		//Index of an MDD node in the dynamic node array for a particular level
typedef int arc_idx;		//Index of an MDD arc  in the dynamic arc  array for a particular level

#endif //TYPES_H
