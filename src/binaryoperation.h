#ifndef BINARY_OPERATION_H
#define BINARY_OPERATION_H

#include <types.h>
#include <handle.h>
#include <operation.h>

class Forest;

class BinaryOperation : public Operation
{
	protected:
	virtual node_idx base_case(node_idx p, node_idx q) const = 0;
	virtual node_idx p_zero(const level k, const node_idx q) const = 0;
	virtual node_idx q_zero(const level k, const node_idx q) const = 0;
	node_idx apply(const level k, const node_idx p, const node_idx q);

	static bool m_commutative;

	public:
	BinaryOperation(Forest* forest);

	fddl_result apply(MDDHandle h1, MDDHandle h2, MDDHandle& result);
};

#endif  //BINARY_OPERATION_H
