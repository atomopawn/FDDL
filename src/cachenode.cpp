/* 
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#include "cachenode.h"
#include "forest.h"

#include <cstdlib>

CacheNode::CacheNode(const node_idx result) : m_result(result), m_next(NULL) 
{}

node_idx CacheNode::result() const
{
	return m_result;
}

CacheNode* CacheNode::next() const
{
	return m_next;
}

void CacheNode::set_next(CacheNode* const& next)
{
	m_next = next;
}

UnaryNode::UnaryNode(const node_idx p, const node_idx result) : CacheNode(result), m_p(p) 
{}

unsigned int UnaryNode::hash() const
{
	return m_p;
}

bool UnaryNode::operator==(const CacheNode& other) const
{
	const UnaryNode* oth = dynamic_cast<const UnaryNode *>(&other);
	if (!oth)
		return false;
	return m_p == oth->m_p;
}

BinaryNode::BinaryNode(const node_idx p, const node_idx q, const node_idx result) : CacheNode(result), m_p(p), m_q(q)
{}

unsigned int BinaryNode::hash() const
{
	//Hash will be unique if 0<=p<=255 and 0<=q<=255.  Otherwise, we will have a collision, but that's okay, too.
	return (m_p << 8) + m_q;
}

bool BinaryNode::operator==(const CacheNode& other) const
{
	const BinaryNode* oth = dynamic_cast<const BinaryNode *>(&other);
	if (!oth)
		return false;
	return m_p == oth->m_p && m_q == oth->m_q;
}

TernaryNode::TernaryNode(const node_idx p, const node_idx q, const node_idx s, const node_idx result) : CacheNode(result), m_p(p), m_q(q), m_s(s)
{ }

unsigned int TernaryNode::hash() const
{
	//Hash will be unique if 0<=p,q,s<=255.  Otherwise, we will have a collision, but that's okay, too.
	return (m_p<<16) + (m_q<<8) + m_s;
}

bool TernaryNode::operator==(const CacheNode& other) const
{
	const TernaryNode* oth = dynamic_cast<const TernaryNode *>(&other);
	if (!oth)
		return false;
	return m_p == oth->m_p && m_q == oth->m_q && m_s == oth->m_s;
}

TupleNode::TupleNode(const node_idx* const vals, const unsigned int size, const node_idx result) : CacheNode(result), m_size(size) {
	m_vals = new node_idx[size];
	for (unsigned int i=0; i<size; ++i) {
		m_vals[i] = vals[i];
	}
}

TupleNode::TupleNode(const TupleNode& other) : CacheNode(other.m_result), m_size(other.m_size) {
	m_vals = new node_idx[m_size];
	for (unsigned int i=0; i < m_size; ++i)
	{
		m_vals[i] = other.m_vals[i];
	}
}

TupleNode::~TupleNode() 
{
	delete[] m_vals;
}

TupleNode& TupleNode::operator=(const TupleNode& other)
{
	if (&other == this)
		return *this;
	m_result = other.m_result;
	m_size = other.m_size;
	delete[] m_vals;
	for (unsigned int i=0; i<m_size; ++i) {
		m_vals[i] = other.m_vals[i];
	}
	return *this;
}

unsigned int TupleNode::hash() const
{
	unsigned int hash = 0;
	for (unsigned int i = 0; i < m_size; ++i) {
		hash = (hash << 8);
		hash += m_vals[i];
	}
	return hash;
}

bool TupleNode::operator==(const CacheNode& other) const
{
	const TupleNode* oth = dynamic_cast<const TupleNode*>(&other);
	if (!oth)
		return false;

	for (unsigned int i=0; i < m_size; ++i) {
		if (m_vals[i] != oth->m_vals[i])
			return false;
	}
	return true;
}

SelectNode::SelectNode(const node_idx p, const node_idx* const vals, const unsigned int size, const node_idx result) : CacheNode(result), m_size(size), m_p(p) {
	m_vals = new node_idx[size];
	for (unsigned int i=0; i<size; ++i) {
		m_vals[i] = vals[i];
	}
}

SelectNode::SelectNode(const SelectNode& other) : CacheNode(other.m_result), m_size(other.m_size), m_p(other.m_p) {
	m_vals = new node_idx[m_size];
	for (unsigned int i=0; i < m_size; ++i)
	{
		m_vals[i] = other.m_vals[i];
	}
}

SelectNode::~SelectNode() 
{
	delete[] m_vals;
}

SelectNode& SelectNode::operator=(const SelectNode& other)
{
	if (&other == this)
		return *this;

	m_result = other.m_result;

	m_p = other.m_p;
	m_size = other.m_size;
	delete[] m_vals;
	for (unsigned int i=0; i<m_size; ++i) {
		m_vals[i] = other.m_vals[i];
	}
	return *this;
}

unsigned int SelectNode::hash() const
{
	unsigned int hash = m_p;
	for (unsigned int i = 0; i < m_size; ++i) {
		hash = (hash << 8);
		hash += m_vals[i];
	}
	return hash;
}

bool SelectNode::operator==(const CacheNode& other) const
{
	const SelectNode* oth = dynamic_cast<const SelectNode*>(&other);
	if (!oth)
		return false;

	if (m_p != oth->m_p)
		return false;

	for (unsigned int i=0; i < m_size; ++i) {
		if (m_vals[i] != oth->m_vals[i])
			return false;
	}
	return true;
}
