/* 
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef CACHE_H
#define CACHE_H

#include <forest.h>

const unsigned int INIT_CACHE_SIZE = 1009;

class CacheNode;

class Cache {
 public:
	Cache();
	~Cache();

	void clear();

	int hit(const int p) const;
	int hit(const int p, const int q) const;
	int hit(const int p, const int q, const int s) const;
	int hit(const int* & vals, const unsigned int num_vals) const;
	int hit(const int p, const int* & vals, const unsigned int num_vals) const;

	void add(const int p, const int r);
	void add(const int p, const int q, const int r);
	void add(const int p, const int q, const int s, const int r);
	void add(const int* & vals, const unsigned int num_vals, const int r);
	void add(const int p, const int* & vals, const unsigned int num_vals, const int r);

 private:
	CacheNode ** m_list;
	unsigned int m_size;
	
	int hit(const CacheNode &) const;
	void add(CacheNode* const &);
};

#endif  //CACHE_H
