/*
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#include "cache.h"
#include "cachenode.h"

typedef std::string CacheException;

Cache::Cache() : m_size(INIT_CACHE_SIZE)
{
	m_list = new CacheNode *[INIT_CACHE_SIZE];
	for (unsigned int i = 0; i < INIT_CACHE_SIZE; ++i)
		m_list[i] = NULL;
}

Cache::~Cache()
{
	clear();
	delete[] m_list;
}

int Cache::hit(const CacheNode &node) const
{
	unsigned int hash = node.hash() % m_size;
	CacheNode* cur = m_list[hash];
	while (cur != NULL) {
		if (*cur == node)
			return cur->result();
		cur = cur->next();
	}
	return -1;
}

int Cache::hit(const int p) const
{
	UnaryNode node(p, -1);
	return hit(node);
}

int Cache::hit(const int p, const int q) const
{
	BinaryNode node(p, q, -1);
	return hit(node);
}

int Cache::hit(const int p, const int q, const int s) const
{
	TernaryNode node(p, q, s, -1);
	return hit(node);
}

int Cache::hit(const int* & vals, const unsigned int size) const
{
	if (size <=0)
		throw CacheException("Hit: not enough values.\n");

	TupleNode node(vals, size, -1);
	return hit(node);	
}

int Cache::hit(const int p, const int* & vals, const unsigned int size) const
{
	if (size <=0)
		throw CacheException("Hit: not enough values.\n");

	SelectNode node(p, vals, size, -1);
	return hit(node);	
}

void Cache::add(CacheNode* const& node) {
	unsigned int hash = node->hash() % m_size;
	node->set_next(m_list[hash]);
	m_list[hash] = node;
}

void Cache::add(const int p, const int r)
{
	UnaryNode* new_node = new UnaryNode(p, r);
	add(new_node);
}

void Cache::add(const int p, const int q, const int r)
{
	BinaryNode* new_node = new BinaryNode(p, q, r);
	add(new_node);
}

void Cache::add(const int p, const int q, const int s, const int r)
{
	TernaryNode* new_node = new TernaryNode(p, q, s, r);
	add(new_node);
}

void Cache::add(const int* & vals, const unsigned int size, const int r)
{
	if (size <=0)
		throw CacheException("Hit: not enough values.\n");

	TupleNode* new_node = new TupleNode(vals, size, r);
	add(new_node);
}

void Cache::add(const int p, const int* & vals, const unsigned int size, const int r)
{
	if (size <=0)
		throw CacheException("Hit: not enough values.\n");

	SelectNode* new_node = new SelectNode(p, vals, size, r);
	add(new_node);
}


void Cache::clear()
{
	CacheNode *prev;

	for (unsigned int i = 0; i < m_size; ++i) {
		while (m_list[i] != NULL) {
			prev = m_list[i];
			m_list[i] = m_list[i]->next();
			delete prev;
		}
	}
}
