/*
 * FDDL : The Free Decision Diagram Library 
 * Copyright (C) 2004 Robert Marmorstein
 * 
 * This program is released under the GNU Public License, version 2.  Please
 * see the file "LICENSE" in the root directory of this repository for more
 * information.
 */

#ifndef FOREST_H
#define FOREST_H

#include <iostream>
#include <assert.h>

#include <types.h>

#include <handle.h>
#include <node.h>
#include <dynarray.h>
#include <cache.h>

#include <max.h>
#include <restrict.h>

inline int 
MAX(const int a, const int b)
{
	return a > b ? a : b;
}

class UniqueTable;
class Cache;

using namespace std;

enum garbage_algorithm { O_LAZY = 0, O_STRICT = 2 };

//The class Forest stores a forest of MDDs with K levels, where K is
//chosen by the user.  By using a node-sharing technique, we store
//multiple MDDs relatively efficiently. 

class Forest {
 protected:
	int K;			//Number of Non-terminal Levels
	int *tail;		//Array [1..K] which records number of arcs per level.
	node_idx *last;		//Array [1..K] which records number of nodes per level.
	int *maxVals;		//Array [0..K] of maximum values for each level.

	//Caches for embedded operations 
	Cache **ProjectCache;
	Cache **MinCache;
	Cache **LessThanCache;
	Cache **ValRestrictCache;
	Cache **CombineCache;
	Cache **ReplaceCache;
	Cache **ProjectOntoCache;
	Cache **ReplaceStrictCache;
	Cache **ShiftCache;
	Cache **PrintCache;
	Cache **ApplyCache;
	Cache **SelectCache;

	int Value(level k, node_idx p, int *tup);
	unsigned int dirtyNodes;

 public:
	void ReallocHandle(MDDHandle & ref);

	void Attach(MDDHandle & ref, node_idx i) {
		node_idx old_idx = ref.index();
		if (old_idx == i)
			return;

		ref = i;
		++((*(*nodes[K])[i]).m_in);
		if (old_idx != -1) {	
			--((*(*nodes[K])[old_idx]).m_in);
		}
	} 

	DynArray < node_idx > **node_remap_array;

	DynArray < Node > **nodes;	//An array [1..K] of heaps of MDD nodes.
	DynArray < node_idx > **arcs;	//An array [1..K] of heaps of MDD arcs.

	MaxOperation* max_op;
	RestrictOperation* restrict_op;

	//Build a new MDD forest of numlevels levels.
	//The domain of each level is specified as an integer range
	//from 0 to maxvals[k] inclusive.

	unsigned int hashnode(const level k, const node_idx p) const;
	int compare(const level k, const node_idx p, const node_idx q) const;

	Forest(int numlevels, int *maxvals);
	~Forest();

	int Last(level k) {
		return last[k];
	}

	int max_val(level k);
	int ChangeMaxVal(level k, int maxval);

	int MakeMDDFromTuple(int *low, int *high, MDDHandle & ref);
	int Assign(MDDHandle root, int *low, int *high, MDDHandle & result);
	int DestroyMDD(MDDHandle mdd);
	int IsElementOf(MDDHandle root, int *tup, bool & result);
	int Value(MDDHandle root, int *tup, int &result);

	int Min(MDDHandle p, MDDHandle q, MDDHandle & result);
	int LessThan(MDDHandle p, int value, MDDHandle & result);
	int Apply(const MDDHandle * roots, const int num_roots,
		  node_idx(*func) (const node_idx *, int), MDDHandle & result);
	int ValRestrict(MDDHandle p, int value, MDDHandle & result);
	int Combine(MDDHandle p, MDDHandle q, int cache_index,
		    MDDHandle & result);
	int Select(MDDHandle p, int num_chains, MDDHandle * chains,
		   MDDHandle & result);

	int Shift(MDDHandle p, level kold, MDDHandle & result);

	int Replace(MDDHandle p, MDDHandle q, bool strict, MDDHandle & result);

	int ProjectOnto(MDDHandle p, MDDHandle q, MDDHandle & result);

	void PrintVals(MDDHandle root, level k);
	node_idx ProjectVals(level k, node_idx p, level cutoff);
	node_idx Projection(level k, node_idx p, level * mask);
	int InternalPrintVals(level k, node_idx p);
	void PrintPort(MDDHandle root, level k);
	void PrintAddy(MDDHandle root, level k);
	void PrintAddy(level k, node_idx p, int *vals, int depth);
	void PrintStates(node_idx root);
	void PrintStates(level k, node_idx root, int *states);

	int CountNodes(node_idx);
	int CountNodes(level k, node_idx p);
	int ComputeMemUsed();
	int RealMemUsed(node_idx);
	int RealMemUsed(level k, node_idx p);

	void ToggleSparsity(bool SparseSwitch);
	void SetGarbageCollection(int alg, int threshold);
	void PruneMDD(MDDHandle p);
	void PrintMDD();
	void PrintMDD(int top, int bottom);

	//Functions to create, manipulate, and destroy MDD nodes.
	node_idx NewNode(level k);
	void DeleteNode(level k, node_idx p);
	node_idx CheckIn(level k, node_idx p);
	int UnpackNode(level k, arc_idx p, int *&fullarray);
	void DeleteDownstream(level k, node_idx p);

	//Set <k:p>[i] = j, updating incoming arc information in the node
	//which the arc currently points to as well as the node we are 
	//assigning the arc to.
	void SetArc(level k, node_idx p, arc_idx i, node_idx j);

	//Do Garbage Collection
	void Compact(level k);
	void CompactTopLevel();

	UniqueTable *UT;	//A special hashtable, the unique table, for storing

	//node information.

	node_idx InternalReplace(level k, node_idx p, node_idx q);
	node_idx InternalProjectOnto(level k, node_idx p, node_idx q);
	node_idx InternalReplaceStrict(level k, node_idx p, node_idx q);
	node_idx InternalMin(level k, node_idx p, node_idx q);
	node_idx InternalLessThan(level k, node_idx p, int value);
	node_idx InternalApply(level k, const node_idx * roots, const unsigned int numroots,
			       node_idx(*func) (const node_idx *, int));
	node_idx InternalValRestrict(level k, node_idx p, int value);
	node_idx InternalCombine(level k, node_idx p, node_idx q,
				 int chain_index);
	node_idx InternalSelect(level k, node_idx p, unsigned int num_chains,
				const node_idx * all_chains);
	node_idx InternalShift(level k, node_idx p, level target);

	int FindRange(level k);
	void InternalPruneMDD(level k, node_idx p);
	void FlushCaches(level k);

	int garbage_alg;
	int garbage_threshold;
	bool sparseEnabled;

	void SaveMDD(char *filename);
	void LoadMDD(char *filename);

	inline Node & FDDL_NODE(level k, node_idx p) const {
		return *((*nodes[k])[p]);
	} 
	
	inline node_idx & FULL_ARC(level k, Node * n, arc_idx i) const {
		return *((*arcs[k])[n->m_down + i]);
	} 
	
	inline arc_idx & SPARSE_INDEX(level k, Node * n, arc_idx i) const {
		return *((*arcs[k])[n->m_down + 2 * i]);
	} 
	
	inline node_idx & SPARSE_ARC(level k, Node * n, arc_idx i) const {
		return *((*arcs[k])[n->m_down + 2 * i + 1]);
	} 
	
	inline node_idx & FDDL_ARC(level k, Node * n, arc_idx i) const {
		return n->sparse() ? SPARSE_ARC(k, n, i) : FULL_ARC(k, n, i);
	}

	inline int top_level() const
	{ 
		return K; 
	}

};
#endif  //FOREST_H
