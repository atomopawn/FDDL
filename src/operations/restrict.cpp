#include "restrict.h"

RestrictOperation::RestrictOperation(Forest* forest) : BinaryOperation(forest)
{ }

node_idx RestrictOperation::base_case(const node_idx p, const node_idx q) const
{
	return q;
}

node_idx RestrictOperation::p_zero(const level k, const node_idx q) const
{
	return q;
}

node_idx RestrictOperation::q_zero(const level k, const node_idx p) const
{
	return p;
}

node_idx RestrictOperation::internal_apply(const level k, const node_idx p, const node_idx q)
{
	return apply(k,p,q);
}
