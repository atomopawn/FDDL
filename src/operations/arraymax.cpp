#include "arraymax.h"

ArrayMaxOperation::ArrayMaxOperation(Forest* forest) : ArrayOperation(forest)
{ }

node_idx ArrayMaxOperation::base_case(const node_idx p[], const unsigned int size) const
{
	node_idx max = p[0];
	for (unsigned int i=1; i < size; ++i) {
		if (p[i] > max)
			max = p[i];
	}
	return max;
}
