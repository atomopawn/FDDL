#include "lessthan.h"
#include "../forest.h"

LessThanOperation::LessThanOperation(Forest* forest, const int value) : UnaryOperation(forest), m_value(value)
{ }

node_idx LessThanOperation::base_case(const node_idx p) const
{
	return p < m_value ? p : 0;
}

node_idx LessThanOperation::zero_case(const level k) const
{
	return 0;
}
