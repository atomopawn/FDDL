#ifndef MAX_H
#define MAX_H

#include <binaryoperation.h>

class Forest;

class MaxOperation : public BinaryOperation {
	private:
	node_idx base_case(const node_idx p, const node_idx q) const;
	node_idx p_zero(const level k, const node_idx q) const;
	node_idx q_zero(const level k, const node_idx p) const;

	public:
	node_idx internal_apply(const level k, const node_idx p, const node_idx q);
	MaxOperation(Forest* forest);
};
#endif //MAX_H
