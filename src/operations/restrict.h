#ifndef RESTRICT_H
#define RESTRICT_H

#include <binaryoperation.h>

class Forest;

class RestrictOperation : public BinaryOperation {
	private:
	node_idx base_case(const node_idx p, const node_idx q) const;
	node_idx p_zero(const level k, const node_idx q) const;
	node_idx q_zero(const level k, const node_idx p) const;

	public:
	node_idx internal_apply(const level k, const node_idx p, const node_idx q);
	RestrictOperation(Forest* forest);
};
#endif //RESTRICT_H
