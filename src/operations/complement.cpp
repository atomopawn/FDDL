#include "complement.h"
#include "../forest.h"

ComplementOperation::ComplementOperation(Forest* forest) : UnaryOperation(forest)
{ }

node_idx ComplementOperation::base_case(const node_idx p) const
{
	return m_forest->max_val(0) - p;
}

node_idx ComplementOperation::zero_case(const level k) const
{
	if (k==0)
		return m_forest->max_val(0);
	node_idx r = m_forest->NewNode(k);
	for (arc_idx i=0; i < m_forest->max_val(k); ++i) {
		m_forest->SetArc(k, r, i, zero_case(k-1));
	}
	r = m_forest->CheckIn(k, r);
	return r;
}
