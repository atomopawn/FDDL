#ifndef ARRAYMAX_H
#define ARRAYMAX_H

#include <arrayoperation.h>

class Forest;

class ArrayMaxOperation : public ArrayOperation {
	private:
	node_idx base_case(const node_idx p[], const unsigned int size) const;

	public:
	node_idx internal_apply(const level k, const node_idx p[], const unsigned int size);
	ArrayMaxOperation(Forest* forest);
};
#endif //ARRAYMAX_H
