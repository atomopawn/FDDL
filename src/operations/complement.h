#include <unaryoperation.h>

class Forest;

class ComplementOperation : public UnaryOperation {
	private:
	node_idx base_case(const node_idx p) const;
	node_idx zero_case(const level k) const;

	public:
	ComplementOperation(Forest* forest);
};
