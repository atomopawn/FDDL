#include <unaryoperation.h>

class Forest;

class PruneOperation : public UnaryOperation {
	private:
	node_idx base_case(const node_idx p) const;
	node_idx zero_case(const level k) const;
	void visit(const level k, const node_idx p);

	public:
	PruneOperation(Forest* forest);
};
