#include "prune.h"
#include "../forest.h"

PruneOperation::PruneOperation(Forest* forest) : UnaryOperation(forest)
{ }

node_idx PruneOperation::base_case(const node_idx p) const
{
	return 0;
}

node_idx PruneOperation::zero_case(const level k) const
{
	return 0;
}

void PruneOperation::visit(const level k, const node_idx p)
{
	m_forest->FDDL_NODE(k,p).set_prune();	
}
