#include "max.h"

MaxOperation::MaxOperation(Forest* forest) : BinaryOperation(forest)
{ }

node_idx MaxOperation::base_case(const node_idx p, const node_idx q) const
{
	return p > q ? p : q;
}

node_idx MaxOperation::p_zero(const level k, const node_idx q) const
{
	return q;
}

node_idx MaxOperation::q_zero(const level k, const node_idx p) const
{
	return p;
}

node_idx MaxOperation::internal_apply(const level k, const node_idx p, const node_idx q)
{
	return apply(k,p,q);
}
