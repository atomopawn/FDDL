#include <unaryoperation.h>

class Forest;

class LessThanOperation : public UnaryOperation {
	private:
	node_idx base_case(const node_idx p) const;
	node_idx zero_case(const level k) const;
	int m_value;

	public:
	LessThanOperation(Forest* forest, const int value);
};
