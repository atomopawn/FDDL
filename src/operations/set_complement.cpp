#include "set_complement.h"
#include "../forest.h"

SetComplementOperation::SetComplementOperation(Forest* forest) : UnaryOperation(forest)
{ }

node_idx SetComplementOperation::base_case(const node_idx p) const
{
	//If p is 0, return 1.  Otherwise(p is not 0), return 0.
	return (p==0);
}

node_idx SetComplementOperation::zero_case(const level k) const
{
	if (k==0)
		return 1;
	node_idx r = m_forest->NewNode(k);
	for (arc_idx i=0; i < m_forest->max_val(k); ++i) {
		m_forest->SetArc(k, r, i, zero_case(k-1));
	}
	return r;
}
