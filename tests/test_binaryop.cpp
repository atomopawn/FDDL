#include <iostream>
#include <cstdlib>
#include "../src/forest.h"
#include "../src/operations/max.h"

using namespace std;

fddl_result test_max(Forest* forest, MDDHandle& result){
	MaxOperation* op = new MaxOperation(forest);
	MDDHandle h1;
	int low1[] = {2,1,0,1};
	int high1[] = {2,1,0,1};
	forest->PrintMDD();
	forest->MakeMDDFromTuple(low1, high1, h1);
	forest->PrintMDD();
	MDDHandle h2;
	int low2[] = {3,2,0,1};
	int high2[] = {3,2,0,1};
	forest->MakeMDDFromTuple(low2, high2, h2);
	forest->PrintMDD();
	return op->apply(h1, h2, result);
}

int main(int argc, char** argv)
{
	int maxvals[] = {3,2,2,2};
	Forest* forest = new Forest(4, maxvals);
	MDDHandle h;
	fddl_result result = test_max(forest, h);
	if (result != SUCCESS || h.invalid()) {
		cout << "Test Max Failed." << endl;
		return EXIT_FAILURE;
	}

	forest->PrintMDD();

	return EXIT_SUCCESS;
}
