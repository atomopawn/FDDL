#include <iostream>
#include <cstdlib>
#include "../src/operations/complement.h"
#include "../src/operations/lessthan.h"
#include "../src/operations/set_complement.h"
#include "../src/forest.h"

using namespace std;

fddl_result test_complement(Forest* forest, MDDHandle& result){
	ComplementOperation* op = new ComplementOperation(forest);
	MDDHandle h;
	int low[] = {2,1,0,1};
	int high[] = {2,1,0,1};
	forest->PrintMDD();
	forest->MakeMDDFromTuple(low, high, h);
	forest->PrintMDD();
	return op->apply(h, result);
}

fddl_result test_lessthan(Forest* forest, MDDHandle& result){
	LessThanOperation* op = new LessThanOperation(forest, 1);
	MDDHandle h;
	int low[] = {2,1,0,1};
	int high[] = {2,1,0,1};
	int low2[] = {1,1,1,1};
	int high2[] = {1,1,1,1};
	int low3[] = {1,1,0,2};
	int high3[] = {1,1,0,2};
	int low4[] = {2,1,1,2};
	int high4[] = {2,1,1,2};

	forest->PrintMDD();
	forest->MakeMDDFromTuple(low, high, h);
	forest->PrintMDD();
	forest->Assign(h, low2, high2, h);
	forest->PrintMDD();
	forest->Assign(h, low3, high3, h);
	forest->PrintMDD();
	forest->Assign(h, low4, high4, h);
	forest->PrintMDD();
	return op->apply(h, result);
}

fddl_result test_set_complement(Forest* forest, MDDHandle& result){
	SetComplementOperation* op = new SetComplementOperation(forest);
	MDDHandle h;
	int low[] = {1,0,0,1};
	int high[] = {1,1,2,2};
	forest->PrintMDD();
	forest->MakeMDDFromTuple(low, high, h);
	forest->PrintMDD();
	return op->apply(h, result);
}

int main(int argc, char** argv)
{
	int maxvals[]= {3,2,2,2};
	Forest* forest = new Forest(4,maxvals);
	MDDHandle h;
	fddl_result result = test_complement(forest, h);
	if (result != SUCCESS || h.invalid()){
		cout << "Test Complement Failed." << endl;
		return EXIT_FAILURE;
	}

	result = test_set_complement(forest, h);
	if (result != SUCCESS || h.invalid()){
		cout << "Test Complement Failed." << endl;
		return EXIT_FAILURE;
	}
	forest->PrintMDD();

	result = test_lessthan(forest, h);
	if (result != SUCCESS || h.invalid()){
		cout << "Test Complement Failed." << endl;
		return EXIT_FAILURE;
	}
	forest->PrintMDD();

	return EXIT_SUCCESS;
}
