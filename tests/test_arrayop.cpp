#include <iostream>
#include <cstdlib>
#include "../src/forest.h"
#include "../src/operations/arraymax.h"

using namespace std;

fddl_result test_arraymax(Forest* forest, MDDHandle& result)
{
	ArrayMaxOperation* op = new ArrayMaxOperation(forest);
	MDDHandle h1;
	MDDHandle h2;
	MDDHandle h3;
	int low1[] = {2,1,0,1};
	int high1[] = {2,1,0,1};
	int low2[] = {1,2,0,1};
	int high2[] = {1,2,0,1};
	int low3[] = {1,1,0,1};
	int high3[] = {1,1,0,1};
	int low4[] = {3,2,0,1};
	int high4[] = {3,2,0,1};
	forest->PrintMDD();
	cout << endl;
	forest->MakeMDDFromTuple(low1, high1, h1);
	forest->PrintMDD();
	cout << endl;
	forest->MakeMDDFromTuple(low2, high2, h2);
	forest->PrintMDD();
	cout << endl;
	forest->MakeMDDFromTuple(low3, high3, h3);
	forest->PrintMDD();
	cout << endl;
	forest->Assign(h3, low4, high4, h3);
	forest->PrintMDD();
	cout << endl;
	MDDHandle handles[3] = {h1, h2, h3};
	return op->apply(handles, 3, result);
}

int main(int argc, char** argv)
{
	MDDHandle h;
	int maxvals[] = {3,2,2,2};
	Forest* forest = new Forest(4, maxvals);
	fddl_result result = test_arraymax(forest, h);
	if (result != SUCCESS || h.invalid()) {
		cout << "Test Array Max Failed." << endl;
		return EXIT_FAILURE;
	}
	forest->PrintMDD();
	return EXIT_SUCCESS;
}
