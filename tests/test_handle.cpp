#include <iostream>
#include "../src/handle.h"

using namespace std;

bool test_constructor()
{
	MDDHandle h;
	if (h.index() != -1)
		return false;
	return true;
}

int main(int argc, char* argv[])
{
	if (!test_constructor()){
		cout << "Test Constructor Failed." << endl;
		return -1;
	}
	return 0;
}
